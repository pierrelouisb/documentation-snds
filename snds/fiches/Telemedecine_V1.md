---
tags:
    - Prestations
    - DCIR/DCIRS 
    - Fiche-programme
---

# Télémédecine
<!-- SPDX-License-Identifier: MPL-2.0 -->

<TagLinks />

La télémédecine est une pratique médicale effectuée par un médecin à distance en mobilisant des technologies de l’information et de la communication.

La télémédecine est constituée de 6 types de prestations :
- La téléconsultation,
- La téléexpertise,
- La télésurveillance,
- Le télésoin,
- La téléassistance médicale,
- La réponse médicale téléphonique.

Les éléments de contexte sont tirés du site Ameli : [Espace Professionnel de santé – Télémédecine](https://www.ameli.fr/rhone/medecin/exercice-liberal/telemedecine) et [Espace Assuré – Télémédecine](https://www.ameli.fr/rhone/assure/remboursements/rembourse/consultations-telemedecine/telemedecine).

## La téléconsultation

### Contexte

La téléconsultation ou consultation à distance est une consultation réalisée par un médecin (généraliste ou de toute autre spécialité médicale), à distance d’un patient, ce dernier pouvant être assisté ou non par un autre professionnel de santé (PS) (ex : médecin, infirmier, pharmacien). Elle peut être suivi d'un acte de téléassistance médicale.

L’ensemble des patients peut bénéficier de téléconsultations, cependant cela relève de la décision du médecin traitant.

Quelques conditions sont à respecter :

- La téléconsultation doit respecter le parcours de soins coordonnés avec orientation préalable du médecin traitant ;  
- Suivi régulier et de qualité des patients (alternance entre présentiel et téléconsultations) ;  
- La téléconsultation doit s’inscrire dans une logique d’ancrage territorial de réponse aux soins.


Par rapport à la consultation, la nécessité de respecter le parcours de soins pour tout recours à une téléconsultation n'est pas requise pour :

- Les patients âgés de moins de 16 ans ;
- L’accès direct spécifique pour certaines spécialités (gynécologie, ophtalmologie, stomatologie, chirurgie orale ou en chirurgie maxillo-faciale, psychiatrie ou neuropsychiatrie et pédiatrie) ;
- Les patients qui ne disposent pas de médecin traitant désigné ou dont le médecin traitant n’est pas disponible dans le délai compatible avec leur état de santé (patients en situation d'urgence), les détenus, les personnes résidant en établissements d’hébergement pour personnes âgées dépendantes (EHPAD) ou établissements accueillant ou accompagnant des personnes adultes handicapées, souvent éloignées de leur domicile initial.

La téléconsultation est obligatoirement réalisée par vidéotransmission, et dans des conditions d’équipement, d’accompagnement et d’organisation adaptées aux situations cliniques des patients permettant de garantir la réalisation d’une consultation de qualité.

L’Assurance Maladie rembourse les actes de téléconsultation depuis le 15 septembre 2018.

La téléconsultation est facturée par le médecin téléconsultant au même tarif qu’une consultation en face-à-face, soit entre 23 € et 58,50 € selon la spécialité et le secteur d’exercice du médecin.

Les règles de prise en charge sont les mêmes que pour une consultation en présentiel, selon les mêmes taux de prise en charge qu’une consultation classique (i.e. 70%).

### Dans le SNDS

La téléconsultation pouvant être réalisée par tous médecins, libéraux ou hospitaliers, ces prestations vont remonter dans le DCIR pour l’activité libérale et dans le PMSI en actes et consultations externes pour les praticiens hospitaliers publics.

| Code PS5 (Prestation DCIR - `PRS_NAT_REF`) | Code B2 (Prestation PMSI ACE - `ACT_COD`) | Libellé | Commentaire |
| ------ | ------ | ------ | ------ |
| 1056 | JC  | TELECONSULTATION GENERALISTE IVG | Versement au PS, prestation non individualisée bénéficiaire |
| 1057 | JCS | TELECONSULTATION SPECIALISTE IVG | Versement au PS, prestation non individualisée bénéficiaire |
| 1096 | TTE | TELECONSULTATION MEDECIN TRAITANT AVEC EHPAD | Arrêté le 29/03/2019 |
| 1157 | TCP | ACTE DE TELECONSULTATION | Arrêté le 31/12/2016 |
| 1164 | TLC | TÉLÉ CONSULTATION - ALD ET / OU EHPAD | Arrêté le 29/03/2019 |
| **1191** | **TC**        | **TELECONSULTATION TOUTES SPECIALITES** | Depuis 2018 |
| **1192** | **TCG** | **TELECONSULTATION GENERALISTE** | Depuis 2018 |
| 3103 | TLS | TELECONSULTATION LORS D'UN SOIN | Depuis fin 2019 |
| 3104 | TLL | TELECONSULTATION LIEU DEDIE | Depuis fin 2019 |
| 3105 | TLD | TELECONSULTATION DOMICILE | Depuis fin 2019 |
| 3308 | TLM | CODE TRACEUR TELECONSULTATION | Versement au PS, prestation non individualisée bénéficiaire |

En 2021, dans l’ESND, les prestations 1191/TC et 1192/TCG représentaient plus de 99% des prestations de téléexpertises dans le DCIR et en ACE dans le PMSI MCO.

La fiche [Téléconsultations](../fiches/teleconsultations.md) propose les éléments nécessaires à la construction d’indicateurs sur la thématique des téléconsultations.

## La téléexpertise

### Contexte

La téléexpertise permet à un médecin, dit « médecin requérant », de solliciter un confrère, dit « médecin requis », en raison de sa formation ou de sa compétence particulière, sur la base d’informations ou d’éléments médicaux liés à la prise en charge d’un patient, et ce, hors de la présence de ce dernier. Les PS non-médicaux peuvent demander une téléexpertise à un PS médical.

La téléexpertise est remboursée pour les patients dans les situations suivantes :

- En affection longue durée (ALD) ;
- Atteints de maladies rares telles que définies par la réglementation en vigueur, dans le cadre de l’organisation des centres de référence maladies rares ;
- Résidant en zones sous denses, telles que définies à l’article 1434-4 du Code de la santé publique et dans lesquelles s’appliquent les aides démographiques conventionnelles ;
- Résidant en EHPAD ou dans des structures médico-sociales ;
- Détenus visés aux articles L. 381-30 et suivants du Code de la sécurité sociale.

Deux niveaux de téléexpertise sont définis :

- Niveau 1 : avis donné sur une question circonscrite, sans nécessité de réaliser une étude approfondie d'une situation médicale.
- Niveau 2 : avis en réponse à une situation médicale complexe après étude approfondie et mise en cohérence.

Pour pouvoir ouvrir droit à la facturation, les patients bénéficiant d’une téléexpertise doivent en principe être connus du médecin requis, afin que celui-ci puisse disposer des informations nécessaires à la réalisation d’un suivi médical de qualité.

Cette connaissance préalable du patient est facultative pour les téléexpertises de niveau 1.

Facturation pour le médecin requis :

- Niveau 1 : la rémunération est de 12 € par téléexpertise (limité à 4 actes par an, par médecin, pour un même patient).
- Niveau 2 : la rémunération est de 20 € par téléexpertise (limité à 2 actes par an, par médecin, pour un même patient).

Facturation pour le médecin requérant :

Forfait annuel (limité à 50 € par an) :
- Niveau 1 : la rémunération est de 5 €.
- Niveau 2 : la rémunération est de 10 €.

Du côté du médecin requérant, aucun acte n'aura besoin d'être facturé.

L'acte de téléexpertise est pris en charge par l'Assurance Maladie obligatoire depuis le 10 février 2019.

### Dans le SNDS

La téléexpertise pouvant être requise et/ou réalisée par tous médecins, libéraux ou hospitaliers, ces prestations vont remonter dans le DCIR pour l’activité libérale et dans le PMSI en actes et consultations externes pour les praticiens hospitaliers publics.


| Code PS5 (Prestation DCIR - `PRS_NAT_REF`) | Code B2 (Prestation PMSI ACE - `ACT_COD`) | Libellé | Commentaire |
| ------ | ------ | ------ | ------ |
| 1097 | TDT | TELE EXPERTISE DOSSIER TRAITANT | Arrêté le 29/03/2019 |
| 1158 | TEP | ACTE DE TELE EXPERTISE| Arrêté le 31/12/2016 |
| 1165 | TLE | TÉLÉ EXPERTISE - ALD ET/OU EHPAD | Arrêté le 29/03/2019 |
| 1166 | TEC | FORFAIT COMPLÉMENTAIRE TÉLÉ EXPERTISE | |
| **1193** | **TE1** | **TELE EXPERTISE DE NIVEAU 1** | Depuis fin 2018 |
| **1194** | **TE2** | **TELE EXPERTISE DE NIVEAU 2** | Depuis fin 2018 |
| 1197 | RQT | FORFAIT REQUERANT | Depuis 2019 |
| 1198 | CRT | FORFAIT REQUERANT COMPLEMENTAIRE | Depuis 2019 |
| 1199 | RQD | DEMANDE TELEEXPERTISE | Depuis 2021 |

En 2021, dans l’ESND, les prestations 1193/TE1 et 1194/TE2 représentaient 100% des prestations de téléconsultations dans le DCIR et en ACE dans le PMSI MCO.


## Télésurveillance

### Contexte

La télésurveillance médicale permet à un PS d’interpréter à distance des données pour le suivi médical d’un patient. L’enregistrement et la transmission de ces données peuvent être automatisés ou réalisés par le patient lui-même ou par le PS.

Toutes les situations médicales sont potentiellement concernées par la télésurveillance. La télésurveillance a d’abord été déployée dans un cadre expérimental. Au 1er juillet 2023, seules les pathologies ayant reçu un avis favorable de la Haute Autorité de santé (HAS) et ayant fait l’objet d’une publication au Journal officiel par arrêté ministériel sont ouvertes à la prise en charge dans le cadre du droit commun. Il s’agit des 4 pathologies suivantes : l’insuffisance cardiaque, l’insuffisance rénale, l’insuffisance respiratoire et le diabète.

Les trois premiers actes créés sont rémunérés sous forme forfaitaire par patient et par semestre (voir la [grille tarifaire](https://www.ameli.fr/sites/default/files/tls_facturationfournisseur.pdf)) :

- Médecins effectuant la télésurveillance (TSM) : entre 36,50 € et 110 € en fonction de la pathologie ;
- PS effectuant l'accompagnement thérapeutique (TSA) : entre 30 € et 60 € ;
- Fournisseur de la solution et des prestations associées (TSF) : entre 225 € et 375 € (hors tarif LPP).

Les nouveaux actes de télésurveillance sont également rémunérés sous forme forfaitaire par patient et par mois :

- Niveau 1 (TVA) : 11 € ;
- Niveau 2 (TVB) : 28 €.

### Dans le SNDS

La télésurveillance pouvant être réalisée par tous PS, libéraux ou hospitaliers, ces prestations vont remonter dans le DCIR pour l’activité libérale et dans le PMSI en actes et consultations externes pour les PS hospitaliers publics.


| Code PS5 (Prestation DCIR - `PRS_NAT_REF`) | Code B2 (Prestation PMSI ACE - `ACT_COD`) | Libellé | Commentaire |
| ------ | ------ | ------ | ------ |
| 1172 | TSA | TELESURVEILLANCE : PS EFFECTUANT L'ACCOMPAGNEMENT | Depuis 2017 |
| 1174 | TSM | TELESURVEILLANCE : MEDECIN TELESURVEILLANT | Depuis 2017 |
| 1190 | TSP | TELESURVEILLANCE : PRIME VARIABLE | Depuis 2018 |
| 1201 | TVA | TELESURVEILLANCE1 | Depuis 2022 |
| 1202 | TVB | TELESURVEILLANCE2 | Depuis 2022 |
| 3593 | TSF | TELESURVEILLANCE : FOURNISSEUR DE LA SOLUTION | Depuis 2017 |
| 3597 | TL1 | TELESURVEILLANCE COMMUN | Depuis 2022 |
| 3601 | TL2 | TELESURVEILLANCE DEROGATOIRE | Depuis 2023 |

En 2021, dans l’ESND, seules les prestations 1172/TSA, 1174/TSM et 3593/TSR remontent dans le DCIR. 

Respectivement, l’accompagnement représente près de 9% des prestations, la surveillance médecin près de 48% et la prestation fournisseur plus de 43%. 

En ACE dans le PMSI MCO, seules les prestations 1172/TSA et 1174/TSM remontent. L’accompagnement représente 9% des prestations et la surveillance médecin 91%.


## Télésoin

### Contexte

Un télésoin permet de bénéficier d’un soin à distance avec un pharmacien ou un auxiliaire médical (ex : effectuer un bilan des traitements médicamenteux, poursuivre un programme de rééducation, réaliser un diagnostic de pédicurie-podologie).


Depuis le 5 juin 2020, tous les actes des orthophonistes inscrits à la [NGAP](../glossaire/NGAP.md) sont réalisables en télésoin, à l’exception des bilans initiaux et des actes nécessitant un contact direct en présentiel avec le patient et/ou un équipement spécifique non disponible auprès du patient.


Les actes en télésoin sont valorisés dans les mêmes conditions que les actes réalisés en présentiel.


### Dans le SNDS

Le télésoin étant réalisé par des PS non médecins, ces prestations vont remonter exclusivement dans le DCIR à partir de 2021.

| Code PS5 (Prestation DCIR - `PRS_NAT_REF`) | Code B2 (Prestation PMSI ACE - `ACT_COD`) | Libellé |
| ------ | ------ | ------ |
| 1086 | TFS | TELESOINS SF |
| 3123 | TMK | TELESOINS MK |
| 3140 | TMI | TELESOINS INFIRMIERS |
| 3141 | TMP | TELESOINS PODOLOGUES |
| **3142** | **TMO** | **TELESOINS ORTHOPHONISTES** |
| 3143 | TMY | TELESOINS ORTHOPTISTES |
| 3402 | TPH | TELESOINS PHARMACIE |

En 2021, dans l’ESND, seule la prestation 3142/TMO remontait dans les données DCIR.

## Téléassistance

La téléassistance médicale a pour objet de permettre à un PS médecin d’assister à distance un autre PS au cours de la réalisation d’un acte.

Cette prestation ne remonte pas dans le SNDS.

## Réponse Médicale Téléphonique

La réponse médicale téléphonique englobe la réponse, les conseils ou l’orientation vers un service de secours.

Ce type d’acte de télémédecine est réalisé notamment par les interlocuteurs répondant aux numéros d’urgence, le 15 ou le 112.

Cette prestation ne remonte pas dans le SNDS.

## Requêtes Types

Dans l’objectif de présenter des requêtes qui s’adaptent facilement aux différentes variantes des langages de bases de données,  il a été choisi d’utiliser le système de gestion de base de données MySQL (système le plus utilisé aujourd’hui).

Sélection des actes de télémédecine réalisées en 2021 dans le DCIR, avec application des filtres recommandés :

```sql
CREATE TABLE telemedecine_DCIR_2021 AS
SELECT PRS.BEN_NIR_PSA, PRS.BEN_RNG_GEM, PRS.EXE_SOI_DTD, PRS.PRS_NAT_REF, PRS.BSE_PRS_NAT, PRS.CPL_PRS_NAT, PRS.CPL_MAJ_TOP, PRS.PSE_SPE_COD, PRS.PSE_STJ_COD, PRS.PRS_ACT_QTE, PRS.PRS_PAI_MNT, PRS.BSE_REM_MNT, PRS.CPL_REM_MNT
FROM ER_PRS_F AS PRS
LEFT JOIN ER_ETE_F as ETE
/* Variables de jointure */
    ON PRS.FLX_DIS_DTD = ETE.FLX_DIS_DTD
    AND PRS.FLX_TRT_DTD = ETE.FLX_TRT_DTD
    AND PRS.FLX_EMT_TYP = ETE.FLX_EMT_TYP
    AND PRS.FLX_EMT_NUM = ETE.FLX_EMT_NUM
    AND PRS.FLX_EMT_ORD = ETE.FLX_EMT_ORD
    AND PRS.ORG_CLE_NUM = ETE.ORG_CLE_NUM
    AND PRS.DCT_ORD_NUM = ETE.DCT_ORD_NUM
    AND PRS.PRS_ORD_NUM = ETE.PRS_ORD_NUM
    AND PRS.REM_TYP_AFF = ETE.REM_TYP_AFF
/* Sélection sur la période de soins */
WHERE PRS.EXE_SOI_DTD >= '2021-01-01' AND PRS.EXE_SOI_DTD <= '2021-12-31'
/* Sélection d'une période de mise à disposition des données d'une année supplémentaire par rapport à la période d'étude. La bonne pratique est de ne pas requêter sur plus de 2 ans par rapport à la période étudiée. */
AND PRS.FLX_DIS_DTD >= '2021-02-01' AND PRS.FLX_DIS_DTD <= '2023-01-01'
/* Sélection des actes de télémédecine (choix d'utiliser PRS_NAT_REF pour sélectionner également les majorations, nécessaires pour calculer des coûts) */
AND PRS.PRS_NAT_REF IN (1056, 1057, 1096, 1157, 1164, 1191, 1192, 3103, 3104, 3105, 3308, 1097, 1158, 1165, 1166, 1193, 1194, 1197, 1198, 1199, 1172, 1174, 1190, 1201, 1202, 3593, 3597, 3601, 1086, 3123, 3140, 3141, 3142, 3143, 3402)
/* Exclusion de l'activité des ES ex-DG (ACE et séjours) qui ne pratiquent pas la facturation directe (données transmises pour information, non exhaustives) */
AND PRS.DPN_QLF NOT IN (71, 72) AND PRS.PRS_DPN_QLP <> 71 
/* Exclusion de l'activité des ES ex-DG (ACE et séjours) en facturation directe (données non exhaustives) */
AND (ETE.ETE_IND_TAA <> 1 OR ETE.ETE_IND_TAA IS NULL);
```

Sélection des actes de télémédecine réalisées en 2021 en ACE dans le PMSI MCO, avec application des filtres recommandés :

```sql
CREATE TABLE telemedecine_DCIR_2021 AS
SELECT C.NIR_ANO_17, F.ETA_NUM, F.SEQ_NUM, F.ACT_NBR, F.ACT_COE, F.PRI_UNI, F.REM_TAU
FROM T_MCO19FCSTC AS F  
INNER JOIN T_MCO19CSTC AS C
    ON F.ETA_NUM = C.ETA_NUM
    AND F.SEQ_NUM = C.SEQ_NUM
/* Exclusion des clés de chaînage incorrectes sur les informations des bénéficiaires */
WHERE C.NIR_RET = '0' AND C.NAI_RET = '0' AND C.SEX_RET = '0' AND C.IAS_RET = '0' AND C.ENT_DAT_RET = '0'
/* Sélection des actes de télémédecine.
Attention, la variable ACT_COD n'est pas codée selon un format homogène selon les établissements. */
AND (TRIM(F.ACT_COD) IN ('TCP', 'TC', 'TCG', 'TDT', 'RQT', 'CRT', 'RQD', 'TSA', 'TSM', 'TSP', 'TSF') OR TRIM(F.ACT_COD) LIKE 'TL%' OR TRIM(F.ACT_COD) LIKE 'TE%' OR TRIM(F.ACT_COD) LIKE 'TV%');
```

La quasi-totalité des actes de télémédecine remontent dans la table `T_MCOaaFCSTC`, table des honoraires en ACE. Quelques rares actes mal codés peuvent remonter dans la table `T_MCOaaFBSTC`, table des prestations en ACE.


::: tip Crédits
Cette fiche a été rédigée en collaboration entre le Health Data Hub et la société HEVA.
:::
