---
tags:
  - Bénéficiaires
  - Prestations
  - Aides fiancières de l'état
  - Régime / Organisme
  - Généralités SNDS
  - DCIR/DCIRS
---

# Indemnités journalières
<!-- SPDX-License-Identifier: MPL-2.0 -->

<TagLinks />

L'abréviation IJ pour indemnités journalières sera utilisée pour cette fiche.

## IJ dans le RG - cas du salarié

En cas d'arrêt maladie, sous certaines conditions et après un délai de carence de trois jours, le salarié peut percevoir des indemnités journalières (IJ). Elles sont calculées sur la base des salaires bruts des 3 ou 12 mois précédant l'arrêt et sont versées tous les 14 jours.

### Conditions d'attribution

> **Arrêt de travail < 6 mois**

Si l'arrêt de travail est < à 6 mois, le salarié doit avoir : 

- travaillé au moins 150 heures au cours des trois mois civils ou des 90 jours précédant l'arrêt de travail ;   
**ou**  
- avoir cotisé sur un salaire au moins égal à 1 015 fois le montant du SMIC horaire au cours des 6 mois civils précédant l'arrêt de travail.  


> **Arrêt de travail > 6 mois**  

Si la durée de l'arrêt de travail est > à 6 mois, le salarié est en arrêt de longue durée, il doit :  

- justifier de 12 mois d'immatriculation en tant qu'assuré social auprès de l'Assurance Maladie à la date de l'arrêt de travail **et** avoir travaillé au moins 600 heures au cours des 12 mois civils ou des 365 jours précédant l'arrêt de travail,  
**ou**  
- ou avoir cotisé sur un salaire au moins égal à 2 030 fois le montant du SMIC horaire au cours des douze mois civils précédant l'arrêt de travail.  

::: warning Cas particulier
La personne exerce **une profession à caractère saisonnier ou discontinu** et elle ne remplit pas les conditions de montant de cotisations ou de durée de travail prévues dans le cas général, elle doit alors :  
-	avoir travaillé au moins 600 heures au cours des 12 mois civils ou des 365 jours consécutifs précédant l'arrêt de travail,
**ou**
-	avoir cotisé sur un salaire au moins égal à 2 030 fois le montant du SMIC horaire au cours des douze mois civils précédant l'arrêt de travail.
- *si la durée de l'arrêt est > à 6 mois*, elle doit également justifier de 12 mois d'immatriculation en tant qu'assuré social auprès de l'Assurance Maladie.
:::

::: danger À noter
La personne peut bénéficier de 360 IJ sur une période maximale de 3 ans. Si elle a une affection de longue durée, elle peut bénéficier d'IJ pendant 3 ans de date à date.
:::

### Montant de l'IJ

L'IJ que la personne recevra pendant l’arrêt de travail est égale à 50 % du salaire journalier de base.  

Celui-ci est calculé sur la moyenne des salaires bruts (= salaires soumis à cotisations) des 3 derniers mois précédant l’arrêt de travail, ou des 12 mois en cas d'activité saisonnière ou discontinue, pris en compte dans la limite de 1,8 fois le SMIC mensuel en vigueur (soit 2 697,25 € au 1er janvier 2018).

Par exemple : sur la base d'un salaire journalier de 75€, l’IJ sera de 37,50€ par jour. 

Si le salarié à au moins 3 enfants à charge, l’IJ est majorée à partir du 31ème jour d'arrêt de travail continu. Elle est alors égale à 66,66 % du salaire journalier de base.

Par exemple : sur la base d'un salaire journalier de 75€,  l’IJ sera de 50 €.

Montant maximum de l’IJ maladie au 1er janvier 2018 :

| Type d'IJ|	Montant |
|----------------------------|----------------------------|
|IJ maladie normale |	44,34 €|
|IJ maladie majorée pour charge de famille à partir du 31ème jour d’arrêt de travail |	59,12 €|

### Délai de carence de 3 jours

Pendant les 3 premiers jours de l’arrêt de travail, les IJ ne sont pas versées, c'est ce que l'on appelle le délai de carence.  
En principe, le délai de carence s'applique au début de chaque arrêt de travail.

**Exceptions** :  
Le délai de carence ne s'applique pas lors d'un arrêt de travail dans les cas suivants :  
-	la reprise d'activité entre deux prescriptions d'arrêt de travail ne dépasse pas 48 heures, à condition que le deuxième arrêt soit prescrit dans le cadre d'une prolongation ;  
-	si la personne est en affection de longue durée et que ses arrêts de travail sont en rapport avec cette maladie, le délai de carence n'est retenu que pour le premier arrêt de travail pour une même période de 3 ans.  

### Cumul avec d’autres revenus

Les IJ peuvent se cumuler notamment avec :  
-	la pension d'invalidité (lorsque le taux d'invalidité de la personne est compatible avec une reprise d'activité professionnelle),  
-	la pension de vieillesse (lorsque le retraité poursuit par ailleurs une activité salariée),  
-	les indemnités de congés payés,  
-	le salaire (si l'employeur le maintient, en tout ou partie, pendant l'arrêt de travail).  

Le cumul est impossible avec :
-	les allocations chômage,  
-	les IJ de maternité,  
-	les IJ d'accident du travail et de maladie professionnelle (AT-MP).  

### Indemnités complémentaires versées par l’employeur

Pour percevoir les indemnités complémentaires, la personne doit remplir toutes les conditions suivantes :
-	justifier d'au moins une année d'ancienneté dans l'entreprise (calculée à partir de votre 1er jour d'absence),  
-	avoir transmis à l'employeur le certificat médical dans les 48 heures,  
-	bénéficier des IJ versées par la Sécurité sociale,    
-	être soigné en France ou dans l'un des États membres de l'Espace économique européen (EEE),  
-	ne pas être travailleur à domicile ou salarié saisonnier, intermittent ou temporaire.  

Si la personne remplit les conditions ouvrant droit aux indemnités complémentaires, elle perçoit 90 % de la rémunération brute qu’elle aurait perçue si elle avait travaillé.  

À partir du 31e jour d'arrêt consécutif, le pourcentage est abaissé aux 2/3 (soit 66,66 %) de sa rémunération.  

Un délai de carence de 7 jours est prévu pour chaque arrêt de travail.  
Ainsi, sauf dispositions conventionnelles ou accord collectif plus favorables, le versement des indemnités complémentaires commence au 8e jour de l'arrêt maladie.  

La durée de versement des indemnités versées par l'employeur varie en fonction de l’ancienneté, de la manière suivante:

|Durée d'ancienneté dans l'entreprise|Durée maximum de versement des indemnités|
|------------------------------------|------------------------------------|
|de 1 à 5 ans | 60 jours (30 jours à 90 % et 30 jours à 66,66 %)|
|de 6 à 10 ans |    80 jours (40 jours à 90 % et 40 jours à 66,66 %)|
|de 11 à 15 ans|    100 jours (50 jours à 90 % et 50 jours à 66,66 %)|
|de 16 à 20 ans|    120 jours (60 jours à 90 % et 60 jours à 66,66 %)|
|de 21 à 25 ans|	140 jours (70 jours à 90 % et 70 jours à 66,66 %)|
|de 26 à 30 ans|	160 jours (80 jours à 90 % et 80 jours à 66,66 %)|
|31 ans et plus|	180 jours (90 jours à 90 % et 90 jours à 66,66 %)|

Si vous avez déjà bénéficié d'une ou plusieurs périodes d'indemnisation pour maladie par l'employeur dans les 12 mois précédents, la durée de versement est déduite du nombre de jours déjà indemnisés.

## IJ dans le RG - cas particuliers

### Cas de la personne sans emploi

Sous conditions et après un délai de carence de 3 jours, la personne sans emploi percevra des IJ pendant l’arrêt maladie. 

Ces sommes sont versées par l'Assurance Maladie pour compenser la perte de ses allocations chômage. 

Elles se calculent sur la base du salaire antérieur à l’admission à Pôle emploi ou à la cessation de son activité depuis moins d’1 an. Elles sont également versées tous les 14 jours.

**Conditions d'attributions**

La personne peut percevoir des IJ si elle est sans emploi et sous réserve de remplir l'une des conditions suivantes :  
-	elle perçoit une allocation de l'assurance chômage ;  
-	elle a été indemnisé par l'assurance chômage au cours des 12 derniers mois ;  
-	elle a cessé son activité salariée depuis moins de 12 mois.  

C'est son activité salariée antérieure qui détermine l'attribution et le calcul des IJ.  
Le calcul s'effectue sur les derniers mois travaillés et non sur l’allocation chômage.  
De même, si elle a cessé son activité, sans bénéficier d'allocations chômage depuis, ses droits seront étudiés sur la base de ses derniers salaires dans la limite d'une année.  

Le montant de l'IJ (normale et majorée) ainsi que le délai de carence sont identiques à ceux du cas général.

### Cas des PAMC en arrêt de travail pendant la grossesse

Si la personne est praticienne ou auxiliaire médicale conventionnée (PAMC), affiliée à titre personnel au régime d'assurance maladie des PAMC, enceinte et qu’elle se trouvez dans l'incapacité physique de continuer ou de reprendre son activité professionnelle en raison de difficultés médicales liées à sa grossesse, elle peut percevoir une IJ forfaitaire maladie, sous réserve de remplir les conditions d'ouverture de droit.

Le montant de l'IJ est identique à celui du cas général (montant de l'IJ normale). Le délai de carence est identique à celui du cas général aussi.

L'IJ forfaitaire maladie peut être versée pendant 87 jours maximum.

## Les indemnités journalières dans le SNDS

Dans le SNDS, les IJ sont repérées via les codes `PRS_NAT_REF` dans la table `ER_PRS_F`.

|`PRS_NAT_REF` | Libellé|
|------------|------------|
|6110|IJ NORMALES +6MOIS|
|6111|IJ NORMALES -3MOIS|
|6112|IJ NORMALES +3MOIS|
|6113|IJ REDUITES -3MOIS|
|6114|IJ REDUITES +3MOIS|
|6115|IJ MAJOREES -3MOIS|
|6116|IJ MAJOREES +3MOIS|
|6117|IJ PARTIELLE, PERTE DE SALAIRE -3MOIS|
|6118|IJ PARTIELLE, PERTE DE SALAIRE +3MOIS|
|6119|IJ MAJOREES +6MOIS|
|6121|IJ PRENATALES|
|6122|IJ POSTNATALES|
|6123|IJ EN CAS D’ADOPTION|
|6124|IJ CONGE SUPPLEMENTAIRE PREMA|
|6212|IJ CONGE MATERNITE AU PERE|
|6120|INDEMNITE TEMPORAIRE D’INAPTITUDE |
|6134|INDEMNITE MALADIE PAMC -3MOIS|
|6135|INDEMNITE MALADIE PAMC +3MOIS|
|6239|INDEMNITE MALADIE DOUBLE ACTIVITE PAMC |

Ces codes prestations sont à croiser avec la nature d’assurance au titre de laquelle les IJ sont versées. 
Il s’agit de la variable `RGO_ASU_NAT` dans la table `ER_PRS_F`.

|`RGO_ASU_NAT` | Libellé|
|------------|------------|
|10 | Maladie|
|30 | Maternité|
|40 | AT/MP|


::: tip Crédits
Cette fiche a été rédigée par Emeline HEYNDRICKX-GALLIN.  
Les ressources suivantes ont été utilisées : [Ameli](https://www.ameli.fr/assure/remboursements/indemnites-journalieres), [site du Service Public](https://www.service-public.fr/particuliers/vosdroits/F3053), [MSA](http://www.msa.fr/lfy/sante/ij-amexa) et [le régime des indépendants](https://www.secu-independants.fr/sante/indemnites-journalieres/).  
:::
