# ANS - Agence du numérique en santé
<!-- SPDX-License-Identifier: MPL-2.0 -->

Elle contribue à l'amélioration du système de santé aux côtés de tous les acteurs, privés comme publics, professionnels ou usagers, grâce à la transformation numérique.

L'ANS a remplacé l'ASIP (Agence française de la santé numérique) en décembre 2019.

## Références

[Site de l'ANS](https://esante.gouv.fr/lagence)
