# HAS - Haute Autorité de Santé
<!-- SPDX-License-Identifier: MPL-2.0 -->

Autorité publique indépendante à caractère scientifique, la Haute Autorité de santé (HAS) vise à développer la qualité dans le champ sanitaire, social et médico-social, au bénéfice des personnes. Elle travaille aux côtés des pouvoirs publics dont elle éclaire la décision, avec les professionnels pour optimiser leurs pratiques et organisations, et au bénéfice des usagers dont elle renforce la capacité à faire leurs choix. Elle a été créée par la loi du 13 août 2004 relative à l’Assurance maladie.

La HAS exerce son activité dans le respect de trois valeurs : la rigueur scientifique, l'indépendance et la transparence.

Elle coopère avec tous les acteurs au service de l’intérêt collectif et de chaque citoyen et porte les valeurs de solidarité et d’équité du système de santé.

# Références

[Site internet de la HAS](https://www.has-sante.fr/)
