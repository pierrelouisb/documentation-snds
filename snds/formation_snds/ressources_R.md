# Ressources pour apprendre à utiliser R sur le portail SNDS
<!-- SPDX-License-Identifier: MPL-2.0 -->  

## Ressources de la Cnam

- ***R - Accès à R sur portail SNDS.pdf*** (dans l’onglet *Accueil* du portail SNDS) : démarche d’accès pour voir apparaître l'icône *RStudio* dans l'onglet *Statistiques*
- ***R - Guide_utilisateur_RStudio_ V1.2.pdf*** (dans l’onglet *Accueil* du portail SNDS) : guide utilisateur de la Cnam. Les informations les plus importantes s’y trouvent.
- ***R - Accès à R sur portail SNDS.pdf*** (dans l’onglet *Accueil* du portail SNDS) : quelques conseils en plus du guide utilisateur
- le **support de la formation R** proposée par la Cnam que l’on trouvera sur le portail (“Tableaux et requêtes” > SAP BusinesObjects > Documents > Dossiers > SNIIRAM > _Documentation SNIIRAM-SNDS > Formation > support des formations SNIIRAM > 11-Logiciels R)

## Autres ressources

  <a href="/files/DREES/tuto_R_portail_SNDS.nb.html" title="Download"> Tutoriel "Pratiquer R sur le portail SNDS"</a> d'un utilisateur de la DREES, qui complète le guide utilisateur de la Cnam

